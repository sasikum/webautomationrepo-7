package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class MedicalKitPages {

	
	public WebElement moveToMedicalKitModule(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[contains(text(),'Medical Kits')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getCreateKitButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[contains(text(),'Create Kit')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getUpdateKitButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='update_kit']/button";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getSaveKitButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[contains(text(),'Save Kit')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getKitNameField(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='userDefinedKitName']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getMedicalKitDropDown(WebDriver driver,ExtentTest logger){
		
		String xpath = "//select[@id='medicalKitListDropDown']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getDeleteKitButton(WebDriver driver,ExtentTest logger){
	
		String xpath = "//div[@id='delete_kit']/button";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getDeleteKitNoButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='cancel']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getDeleteKitPopUp(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@class='sweet-alert showSweetAlert visible']/p";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getDeleteKitYesButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='confirm']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getGeneralAdviceTextArea(WebDriver driver,ExtentTest logger){
		
		String xpath = "//textarea[@id='genericAdvice']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getGeneralAdviceDropDown(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='dropdown_btn']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement select1stDropDown(WebDriver driver,ExtentTest logger){
		
		String xpath = "//ul[@id='dropdown']/li[1]/input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getAddGeneralAdviceTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='toggleTemplate']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getAddAsTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='btn btn-default takephotobtn']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getSaveTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='generalAdviceTemplateNameModal']//button[@id='submitTemplateForm']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getGeneralAdviceTemplateName(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='generalAdviceTemplateName']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getLabTestIdAttribute(WebDriver driver,ExtentTest logger){
		
		String xpath = "//table[@id='tosTable']/tbody/tr[1]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	
	public WebElement getOpenConsultationsModule(WebDriver driver,ExtentTest logger) {
		String xpath = "//a[contains(text(),'Open Consultations')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getPatientSearchButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[@id='search']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getPatientIdSearch(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@placeholder='Enter Patient Id']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public  WebElement getYesInCloseConsultationModal(WebDriver driver,ExtentTest logger){
		String xpath ="//button[@class='confirm']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllCheckoutElements(WebDriver driver,ExtentTest logger){
		String xpath ="//a[contains(text(),'Check out')]";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}

	public WebElement getNoOpenConsultationsText(WebDriver driver,ExtentTest logger) {
		String xpath ="//center[contains(text(),'No Open Consultation Found.')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getSelectMedicalKit(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='medicalKitSearch']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	

		


		
		Web_GeneralFunctions.sendkeys(element, value, "sending created medical kit in practice management", driver, logger);

		Thread.sleep(1000);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
		
		return element;
	}
	
	
	public WebElement getApplyMedicalKit(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='medicalKitApplyButton']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Medical kit created successfully.')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getMedicalKitHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),'Medical Kit')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSymptomsSection(WebDriver driver,ExtentTest logger) {
		String xpath = "//div[@id='symptomsKit']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisSection(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='diagnosisAndPrescriptionKit']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getGeneralAdviceSection(WebDriver driver,ExtentTest logger) {
		String xpath="//div[@id='generalAdviceTemplateId']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTestSection(WebDriver driver,ExtentTest logger) {
		String xpath= "//div[@id='labTestsKit']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPrescriptionSection(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='Prescription']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSymptomDeleteButton(WebDriver driver,int row,ExtentTest logger) {
		String xpath ="//button[@id='delete_row"+row+"']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisDeleteButton(WebDriver driver,int row,ExtentTest logger) {
		String xpath ="(//button[@class='text-center btn btn-danger diagnosisFields'])["+row+"]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement GetPrescriptionDeleteButton(WebDriver driver,int row,ExtentTest logger) {
		String xpath ="(//span[@class='glyphicon glyphicon-remove btnDelete_def gray-icon'])["+row+"]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getGeneralAdviceTemplateNamesAsList(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@class='dropdown_checkbox']//parent::li//label";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public WebElement getTestScanRemoveButton(WebDriver driver,int row, ExtentTest logger) {
	 String xpath ="(//button[@class='text-center btn btn-danger deleteTestOrScan'])["+row+"]";
	 return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
	}
	
	public List<WebElement> getAllSelectedDiagnosisElements(WebDriver driver,ExtentTest logger) {
		String xpath= "//table[@id='diagnosisNewTable']//input";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllSelectedSymptomElements(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@placeholder='Symptom name']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getSelectedPrescriptionElements(WebDriver driver,ExtentTest logger){
		String xpath ="//textarea[@placeholder='Search Medicine']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getSelectedTestScanElements(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@name='testNameTestOrScan']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllMedicalkitNamesFromDropdown(WebDriver driver,ExtentTest logger){
		String xpath ="//select[@id='medicalKitListDropDown']//option";
		List<String> medicalKitNames = new ArrayList<>();
		List<WebElement> medicalKits = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement kit :medicalKits) {
			medicalKitNames.add(kit.getText());
		}
		return medicalKits;
	}
	
	public WebElement getSymptomTextField(WebDriver driver,int row,ExtentTest logger){
		
		String symptomField = "//input[@id='Symptom"+ row+"']";
		return(Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger));
		
	}
	public WebElement getDiagnosisTextField(WebDriver driver,int row,ExtentTest logger){
		
		String symptomField = "//input[@id='DiagnosisName"+row+"']";
		return(Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger));
		
	}
	
	public List<WebElement> getAllElementsAsList(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	
	public List<String> getAllSymptomSuggestionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> symptomsList  = new ArrayList<>();
		List<WebElement> symptomElements = getAllElementsAsList(driver,logger);
		for(WebElement e :symptomElements) {
			symptomsList.add(Web_GeneralFunctions.getText(e, "extract symptom text", driver, logger));
		}
		return symptomsList;
	}
	
	public List<WebElement> getAllSymptomRows(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@placeholder='Symptom name']";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<String> getAllDiagnosiSuggestionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> diagnosisList  = new ArrayList<>();
		List<WebElement> diagnosisElements = getAllElementsAsList(driver,logger);
		for(WebElement e : diagnosisElements) {
			diagnosisList.add(Web_GeneralFunctions.getText(e, "extract diagnosis text", driver, logger));
		}
		return diagnosisList;
	}
	
	public List<WebElement> getAllDiagnosisRows(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@placeholder='Diagnosis']";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public WebElement getPrescriptionTextField(WebDriver driver,int row,ExtentTest logger){
		
		String symptomField = "//textarea[@id='drugName"+row+"']";
		return(Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger));
		
	}
	
	public List<String> getAllPrescriptionSuggestionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> prescriptionsList  = new ArrayList<>();
		List<WebElement> prescriptionElements = getAllElementsAsList(driver,logger);
		for(WebElement e : prescriptionElements) {
			prescriptionsList.add(Web_GeneralFunctions.getText(e, "extract prescription text", driver, logger));
		}
		return prescriptionsList;
	}
	
	public List<WebElement> getAllPrescriptionRows(WebDriver driver,ExtentTest logger){
		String xpath ="//textarea[@placeholder='Search Medicine']";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public WebElement getGeneralAdviceSearchElement(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@placeholder='Search']";
	return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getLabTestTextField(WebDriver driver,int row,ExtentTest logger){
		
		String symptomField = "//input[@id='testOrScanSearchString"+row+"']";
		return(Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger));
		
	}
	
	public List<String> getAllLabTestSuggestionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> labTestsList  = new ArrayList<>();
		List<WebElement> labTestElements = getAllElementsAsList(driver,logger);
		for(WebElement e : labTestElements) {
			labTestsList.add(Web_GeneralFunctions.getText(e, "extract labtest text", driver, logger));
		}
		return labTestsList;
	}
	
	public WebElement getSavedMedicalKit(String kitname, WebDriver driver,ExtentTest logger) {
		String xpath ="//select[@id='medicalKitListDropDown']//option[contains(text(),'"+kitname+"')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getLabTestRows(WebDriver driver,ExtentTest logger){
		String xpath ="//input[@placeholder='Search Test Or Scan']";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public WebElement getSavedGeneralAdviceElementInDropDown(String templateName,WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='dropdown-menu dropdown-menu_custom_tool ui-sortable']//li//label[contains(text(),'"+templateName+"')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	

	public List<WebElement> getAllSymptomRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='containerForsymptoms']//tbody//td//button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<WebElement> getAllDiagnosisRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='diagnosisNewTable']//tbody//td//button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<WebElement> getAllPrescriptionRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='medicine_table']//tbody/tr/th/button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<WebElement> getAllLabTestRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='testOrScanList']//tbody//th//button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public WebElement getLabTestHeaderInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),'Lab Tests')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisHeaderInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),' Diagnosis')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPrescriptionHeaderInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='prescriptionMasterFormContainer']/h4[contains(text(),'Prescription')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPrescriptionTemplateSearch(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='templateSearchField']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTestTemplateSearch(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='labTestsTemplateSearchField']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	
	
	
}

