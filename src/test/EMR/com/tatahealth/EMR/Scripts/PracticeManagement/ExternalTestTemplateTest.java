
package com.tatahealth.EMR.Scripts.PracticeManagement;
import static org.testng.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.LabTestModuleTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.PracticeManagement.ExternalTestTemplatePages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class ExternalTestTemplateTest {
	
	public static String templateName = "";
	public static String testName ="";
	public static String labNotes ="";
	public static List<String>  savedLabTests =new ArrayList<>();
	public static List<String>  savedLabNotes =new ArrayList<>();
	LabTestPage lPage = new LabTestPage();
	ExternalTestTemplatePages pages = new ExternalTestTemplatePages();
	ExtentTest logger;
	PracticeManagementPages pmp = new PracticeManagementPages();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	SymptomPage sp = new SymptomPage();	
	
	
		@BeforeClass(alwaysRun=true,groups= {"Login","Regression"})
		public static void beforeClass() throws Exception {
			Login_Doctor.executionName="EMR_PracticeManagement_ExternalTests";
			Login_Doctor.LoginTest();
			new MedicalKItTest().closePreviousConsultations();
			new PracticeManagementTest().moveToPracticeManagement();
		} 
		
		@AfterClass(groups = {"Login","Regression"})
		public static void afterClass() {
			Login_Doctor.LogoutTest();
			}
		
		//PM_52 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=457)
	public synchronized void moveToExternalTestModuleAndValidate(){
		logger = Reports.extent.createTest("EMR move to external tests module and validate");
		assertTrue(Web_GeneralFunctions.isDisplayed(pages.moveToExternalTestTemplate(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(pages.moveToExternalTestTemplate(Login_Doctor.driver, logger), "Move to external test template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getCreateTemplateButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(pages.getCreateTemplateButton(Login_Doctor.driver, logger)));
	}
	
	//PM_56 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=458)
		public synchronized void validateSectionsInExternalTestsModule()throws Exception{
			logger = Reports.extent.createTest("EMR Validate individual sections in External Tests Module");
			clickCreateTemplate();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getTemplateNameSection(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getSaveTemplateButton(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(pages.getTemplateNameSection(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(pages.getTestNameSection(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(pages.getSaveTemplateButton(Login_Doctor.driver, logger)));
		}
		
		//PM_62 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=459)
		public synchronized void saveTemplateWithEmptyTestDetail()throws Exception{
			logger = Reports.extent.createTest("EMR save template with empty data");
			Web_GeneralFunctions.click(pmp.getLeftMainMenu(Login_Doctor.driver, logger), "click on left menu", Login_Doctor.driver, logger);
			clickSaveTemplate();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lPage.getAddAsTemplateErrorMessage(Login_Doctor.driver, logger));
			String text = Web_GeneralFunctions.getText(lPage.getAddAsTemplateErrorMessage(Login_Doctor.driver, logger), "Get lab test error message", Login_Doctor.driver, logger);
			assertTrue(text.equalsIgnoreCase("Please fill all mandatory fields before adding"));
			Web_GeneralFunctions.wait(2);
		}
		
		//PM_65 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=460)
		public synchronized void clickAddRowWithEmptyData()throws Exception{
			logger = Reports.extent.createTest("EMR click add new row with existing empty row");
			Web_GeneralFunctions.click(pages.getAddrowButton(Login_Doctor.driver, logger), "click on Add row button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lPage.getMessage(Login_Doctor.driver, logger));
			String text = Web_GeneralFunctions.getText(lPage.getMessage(Login_Doctor.driver, logger), "Get Lab test empty data error message", Login_Doctor.driver, logger);
			assertTrue(text.equalsIgnoreCase("Please fill all mandatory fields before adding"));
			
		}
	
		//PM_57 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=461)
		public synchronized void validateRelatedLabTestsDisplay() throws Exception {
			logger = Reports.extent.createTest("EMR validate display of related lab test names");
			String labTestSearchText ="blood";
			Web_GeneralFunctions.sendkeys(pages.getLabTestTextField(Login_Doctor.driver, logger), labTestSearchText, "send labTest search text", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getAllElementsAsList(Login_Doctor.driver, logger).get(1));
			List<String> labTestsList = pages.getAllLabTestSuggestionsAsList(Login_Doctor.driver, logger);
			for(String s:labTestsList) {
				assertTrue(s.toLowerCase().contains(labTestSearchText));
			}
			Web_GeneralFunctions.clearWebElement(pages.getLabTestTextField(Login_Doctor.driver, logger),"clear lab test field",Login_Doctor.driver,logger);
			
		}
	
	//PM_60, PM_58 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=462)
	public synchronized void enterCustomLabNotesAndCustomLabTest() throws Exception {
		logger = Reports.extent.createTest("EMR Enter custom lab tests and lab notes");
		testName = "!@#$%^&*()_+-=_{'}><?/"+RandomStringUtils.randomAlphanumeric(185);
		Web_GeneralFunctions.sendkeys(pages.getLabTestTextField(Login_Doctor.driver, logger),testName, "Sending RandomText to test Name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String maxlengthOfLabTest = Web_GeneralFunctions.getAttribute(pages.getLabTestTextField(Login_Doctor.driver, logger), "maxlength", "getting max allowed characters length", Login_Doctor.driver, logger);
		assertTrue(Integer.parseInt(maxlengthOfLabTest)==200);
		labNotes ="!@#$%^&*()_+-=_{'}><?/"+RandomStringUtils.randomAlphanumeric(185);
		Web_GeneralFunctions.sendkeys(pages.getLabNotesField(Login_Doctor.driver, logger),labNotes, "Sending RandomText to test Name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String maxlengthOfLabNotes = Web_GeneralFunctions.getAttribute(pages.getLabNotesField(Login_Doctor.driver, logger), "maxlength", "getting max allowed characters length", Login_Doctor.driver, logger);
		assertTrue(Integer.parseInt(maxlengthOfLabNotes)==200);
	}
	
	//PM_61 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=463)
		public synchronized void saveTemplateWithEmptyTemplateName()throws Exception{
			logger = Reports.extent.createTest("EMR save template with empty template name");
			String atr = Web_GeneralFunctions.getAttribute(pages.getLabTestIdAttribute(Login_Doctor.driver, logger), "id", "get id attribute", Login_Doctor.driver, logger);
			LabTestModuleTest.row = Integer.parseInt(atr);
			clickSaveTemplate();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getEnterTemplateNameMessage(Login_Doctor.driver, logger));
			String text = Web_GeneralFunctions.getText(pages.getEnterTemplateNameMessage(Login_Doctor.driver, logger), "Get error message", Login_Doctor.driver, logger);
			assertTrue(text.equalsIgnoreCase("Please enter Template Name"));
		}

	//PM_59 covered here 
	@Test(groups= {"Regression","PracticeManagement"},priority=464)
	public synchronized void enterTemplateName() throws Exception {
		logger = Reports.extent.createTest("EMR validate Template Name field Limitation");
		templateName = RandomStringUtils.randomAlphanumeric(130)+"!@#$%^&*()_+-=_{'}><?/";
		Web_GeneralFunctions.sendkeys(pages.getTemplateNameTextField(Login_Doctor.driver, logger),templateName, "Sending RandomText to template Name field", Login_Doctor.driver, logger);
		String maxlength = Web_GeneralFunctions.getAttribute(pages.getTemplateNameTextField(Login_Doctor.driver, logger), "maxlength", "getting max allowed characters length", Login_Doctor.driver, logger);
		assertTrue(Integer.parseInt(maxlength)==150);
	}
	
	
	//PM_64 , PM_55 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=465)
	public synchronized void saveTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR save template ");
		clickSaveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(pages.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Template saved successfully."));
		Web_GeneralFunctions.wait(2);
	}
	
	//PM_54,PM_53 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=466)
		public synchronized void verifySavedTemplatePresentInList()throws Exception{
			
			logger = Reports.extent.createTest("EMR saved template present in list");
			Web_GeneralFunctions.sendkeys(pages.getTemplateSearchField(Login_Doctor.driver, logger), templateName.substring(0,5), "Sending templateName to search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger));
			String savedTemplateName = Web_GeneralFunctions.getText(pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger), "get templateName text", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger), "clicking on first template element", Login_Doctor.driver, logger);
			/*This assertion is failing due to manual issue- Not able to enter all special characters*/
			//assertTrue(savedTemplateName.equalsIgnoreCase(templateName.substring(0,150));
			}
	
		//PM_59,PM_54 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=467)
	public synchronized void validateTempalteNameAndLabTestNameFieldLimitations() throws Exception {
		logger = Reports.extent.createTest("EMR validate templateName and labtest name field limitations");
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getUpdateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getCreateTemplateButton(Login_Doctor.driver, logger));
		String savedLabtestName = Web_GeneralFunctions.getAttribute(pages.getLabTestTextField(Login_Doctor.driver, logger), "value", "extract saved lab test name", Login_Doctor.driver, logger);
		String savedLabNote = Web_GeneralFunctions.getAttribute(pages.getLabNotesField(Login_Doctor.driver, logger),"value","extract Lab notes",Login_Doctor.driver,logger);
		assertTrue(savedLabtestName.equals(testName.substring(0, 200)));
		assertTrue(savedLabNote.equals(labNotes.substring(0,200)));
		assertTrue(savedLabtestName.length()==200);
		assertTrue(savedLabNote.length()==200);
	}
	
	//PM_63 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=468)
	public synchronized void saveTemplateWithNotesAndEmptyLabTest()throws Exception{
		logger = Reports.extent.createTest("EMR save template with notes and empty lab test name");
		LabTestModuleTest lTest = new LabTestModuleTest();
		lTest.clickAddRow();
		LabTestModuleTest.row++;
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getLabNotesTextField(Login_Doctor.driver, LabTestModuleTest.row, logger));
		String notes = RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(pages.getLabNotesTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), notes, "send notes text", Login_Doctor.driver, logger);
		clickUpdateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lPage.getAddAsTemplateErrorMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(lPage.getAddAsTemplateErrorMessage(Login_Doctor.driver, logger), "Get lab test error message", Login_Doctor.driver, logger);
		//This assertion is failing because of manual issue
		//assertTrue(text.equalsIgnoreCase("Please fill all mandatory fields before adding"));
		String test ="Blood";
		Web_GeneralFunctions.clear(pages.getLabTestTextField(Login_Doctor.driver, logger), "click on lab test text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pages.getLabTestTextField(Login_Doctor.driver,LabTestModuleTest.row, logger),test, "Sending RandomText to test Name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getLabTestFromDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pages.getLabTestFromDropDown(Login_Doctor.driver, logger), "select first lab test", Login_Doctor.driver, logger);
		clickUpdateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lPage.getAddAsTemplateErrorMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(pages.getTemplateSearchField(Login_Doctor.driver, logger), "clear template name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pages.getTemplateSearchField(Login_Doctor.driver, logger), templateName.substring(0,5), "Sending templateName to search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger), "clicking on first template element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getUpdateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getCreateTemplateButton(Login_Doctor.driver, logger));
		savedLabTests = pages.getAllSavedLabTestNamesAsList(Login_Doctor.driver, logger);
		savedLabNotes = pages.getAllSavedLabNotesAsList(Login_Doctor.driver, logger);
	
	}
	
	//PM_55 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=469)
	public synchronized void DuplicateTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR validate duplicate template name error message ");
		clickCreateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getSaveTemplateButton(Login_Doctor.driver, logger));
		String atr = Web_GeneralFunctions.getAttribute(pages.getLabTestIdAttribute(Login_Doctor.driver, logger), "id", "get id attribute", Login_Doctor.driver, logger);
		LabTestModuleTest.row = Integer.parseInt(atr);
		String freeText = RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(lPage.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), freeText, "sending free text in lab test", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(lPage.getTestScanTemplateNameField(Login_Doctor.driver, logger), templateName, "sending template name", Login_Doctor.driver, logger);
		clickSaveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(pages.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Template name already exists"));
	}
	
//PM_66,PM_54 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=470)
	public synchronized void updateTemplateInLabAndValidate()throws Exception{
		logger = Reports.extent.createTest("EMR update Template present in list");
		Web_GeneralFunctions.clearWebElement(lPage.getLabTestTemplateSearch(Login_Doctor.driver, logger), "clear lab test template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pages.getTemplateSearchField(Login_Doctor.driver, logger),templateName.substring(0,5), "serching saved template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger), "selecting saved template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getUpdateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getCreateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pages.getFirstRemoveRowButtom(Login_Doctor.driver, logger), "clicking on delete row button", Login_Doctor.driver, logger);
		clickUpdateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(pages.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(pages.getTemplateSearchField(Login_Doctor.driver, logger), "clear template name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pages.getTemplateSearchField(Login_Doctor.driver, logger),templateName.substring(0,5), "serching saved template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pages.getFirstTemplateFromDropdown(Login_Doctor.driver, logger), "selecting saved template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getUpdateTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getCreateTemplateButton(Login_Doctor.driver, logger));
		LabTestModuleTest.row =0;
		testName = Web_GeneralFunctions.getAttribute(pages.getLabTestTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab Test Name", Login_Doctor.driver, logger);
		labNotes = Web_GeneralFunctions.getAttribute(pages.getLabNotesTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "extract lab notes", Login_Doctor.driver, logger);
		List<String> updatedLabTests = pages.getAllSavedLabTestNamesAsList(Login_Doctor.driver, logger);
		List<String> updatedLabNotes = pages.getAllSavedLabNotesAsList(Login_Doctor.driver, logger);
		assertTrue(updatedLabTests.size()<savedLabTests.size());
		assertTrue(!updatedLabTests.equals(savedLabTests));
		assertTrue(updatedLabNotes.size()<savedLabNotes.size());
		assertTrue(!updatedLabNotes.equals(savedLabNotes));
		assertTrue(text.equalsIgnoreCase("Template updated successfully"));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=471)
	public synchronized void applyCreatedTemplateInConsultationPage()throws Exception{
		logger = Reports.extent.createTest("EMR apply lab test template in consultation page");
		LabTestModuleTest lTest = new LabTestModuleTest();
		PracticeManagementTest pmt = new PracticeManagementTest();
		VitalMasterTest vmt = new VitalMasterTest();
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		lTest.consultation_014();
		LabTestModuleTest.createTemplate = templateName.substring(0,8);
		lTest.createdTemplateUpdatedInTemplateList();
		Web_GeneralFunctions.wait(3);
		LabTestModuleTest.row=1;
		String labTestName = Web_GeneralFunctions.getAttribute(lPage.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "extract lab test name", Login_Doctor.driver, logger);
		String labNote = Web_GeneralFunctions.getAttribute(lPage.getNotes(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "extract lab notes", Login_Doctor.driver, logger);
		backToExternalLabbTestTemplateModule();
		assertTrue(labTestName.equalsIgnoreCase(testName));
		assertTrue(labNote.equalsIgnoreCase(labNotes));
	}
	
	
	public synchronized void backToExternalLabbTestTemplateModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR Back to prescription template from consultation page");
		PracticeManagementTest pmt = new PracticeManagementTest();
		pmt.moveToPracticeManagement();
		moveToExternalTestModuleAndValidate();
	}
	
	public synchronized void clickCreateTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR click create template");
		Web_GeneralFunctions.click(pages.getCreateTemplateButton(Login_Doctor.driver, logger), "Click create template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getSaveTemplateButton(Login_Doctor.driver, logger));
	}
	
	public synchronized void clickSaveTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR click save template");
		Web_GeneralFunctions.click(pages.getSaveTemplateButton(Login_Doctor.driver, logger), "Click save template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getMessage(Login_Doctor.driver, logger));
	}
	
	public synchronized void clickUpdateTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR click update template");
		Web_GeneralFunctions.click(pages.getUpdateTemplateButton(Login_Doctor.driver, logger), "Click update template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pages.getMessage(Login_Doctor.driver, logger));
	}
}

