package com.tatahealth.EMR.Scripts.Consultation;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabPeriodicityPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabTestClearOptions {
	public static LabPeriodicityPages lab = new LabPeriodicityPages();
	public static ConsultationTest consult = new ConsultationTest();
	public static ExtentTest logger;
	public static String patientName = "Maha";
	public static Integer availableSlot = 0;
	public static String prevSlotId = "";
	public static String slotId = "";
	
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "Lab Test Periodicity";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
		consult.labStartConsultation("Maha");
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.getLabTest(Login_Doctor.driver, logger), "Scroll page till Lab test is visible on the page", 
				Login_Doctor.driver, logger);
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.driver.quit();
		Reports.extent.flush();
	}	
	@Test(priority = 13, groups = { "Regression", "LabPeriodicity" }, description = "LTP_17, LTP_18")
	public void clear_Option() throws Exception {
		logger = Reports.extent.createTest("To verify Clear option is displaying under For textfield");
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		if (lab.getCleanOptionBtn(Login_Doctor.driver, logger).isDisplayed()) {
			lab.getCleanOptionBtn(Login_Doctor.driver, logger).click();
		}
		System.out.println("===================>"+lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertTrue(!lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty(), "Search Test Or Scan field is cleared");
		Assert.assertTrue(!lab.getNotesTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty(), "Notes Test Or Scan field is cleared");
	}
	@Test(priority = 14, groups = { "Regression", "LabPeriodicity" }, description = "LTP_23, LTP_24")
	public void periodicity_time() throws Exception {
		logger = Reports.extent.createTest("To verify Repeat Follow up is present for CFC clinics");
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScanLast(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		try {
			String actualToastMessage = js.executeScript("return document.getElementsByClassName('toast-message')").toString();
			Assert.assertTrue(!actualToastMessage.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Thread.sleep(5000);
	}
	@Test(priority = 15, groups = { "Regression", "LabPeriodicity" }, description = "LTP_25")
	public void calculated_Date() throws Exception {
		logger = Reports.extent.createTest("To verify time duration or calculated date is displaying for the Next test on functionality");
		String number = RandomStringUtils.randomNumeric(1);
		lab.getEveryTextfield2(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown = new Select(lab.getEveryDropdown2(Login_Doctor.driver, logger));
		textDropdown.selectByIndex(2);
		lab.getForTextfield2(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown = new Select(lab.getForDropdown2(Login_Doctor.driver, logger));
		forTextDropdown.selectByIndex(3);
		Assert.assertTrue(lab.getcalculatedDate2(Login_Doctor.driver, logger).getText()!=null, "The calculated date or time duration displayed for the Next test on functionality");
		Thread.sleep(5000);
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
	}
	@Test(priority = 16, groups = { "Regression", "LabPeriodicity" }, description = "LTP_26, LTP_27")
	public void repeat_Follow_Up_Two_Tests() throws Exception {
		logger = Reports.extent.createTest("To verify when patient comes for appointment for the first time and suggest a lab followup for 2 Lab tests");
		String number = RandomStringUtils.randomNumeric(1);
		String number2 = RandomStringUtils.randomNumeric(1);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR Automation Testing";
		Web_GeneralFunctions.wait(2);
		js.executeScript("arguments[0].click()", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Web_GeneralFunctions.wait(3);
		Select textDropdown = new Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		textDropdown.selectByIndex(2);
		lab.getForTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Web_GeneralFunctions.wait(3);
		Select forTextDropdown = new Select(lab.getForDropdown(Login_Doctor.driver, logger));
		forTextDropdown.selectByIndex(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		js.executeScript("arguments[0].value='Total Cholesterol';", lab.getSearchTestOrScan2(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan2(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield2(Login_Doctor.driver, logger).sendKeys(number2);
		Select textDropdown2 = new Select(lab.getEveryDropdown2(Login_Doctor.driver, logger));
		textDropdown2.selectByIndex(2);
		lab.getForTextfield2(Login_Doctor.driver, logger).sendKeys(number2);
		Select forTextDropdown2 = new Select(lab.getForDropdown2(Login_Doctor.driver, logger));
		forTextDropdown2.selectByIndex(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		Assert.assertTrue(lab.getcalculatedDate2(Login_Doctor.driver, logger).getText()!=null, "The calculated date or time duration displayed for the Next test on functionality");
		Assert.assertTrue(lab.getcalculatedDate2(Login_Doctor.driver, logger)!=null, "The calculated date or time duration displayed for the Next test on functionality");
	}
	@Test(priority = 17, groups = { "Regression", "LabPeriodicity" }, description = "LTP_28")
	public void repeat_Follow_Up_Test() throws Exception {
		logger = Reports.extent.createTest("To verify next follow up date is fetched and displaying for the lab test B");
		String number = RandomStringUtils.randomNumeric(1);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR Automation Testing";
		Web_GeneralFunctions.wait(5);
		  js.executeScript("arguments[0].click()",
		  lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		  js.executeScript("arguments[0].value='Urine Analysis';",
		  lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		  lab.getNotesTestOrScan(Login_Doctor.driver,
		  logger).sendKeys(labTamplateText); lab.getEveryTextfield(Login_Doctor.driver,
		  logger).sendKeys(number); Select textDropdown = new
		  Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		  textDropdown.selectByIndex(2); lab.getForTextfield(Login_Doctor.driver,
		  logger).sendKeys(number); Select forTextDropdown = new
		  Select(lab.getForDropdown(Login_Doctor.driver, logger));
		  forTextDropdown.selectByIndex(3);
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		lab.getlabTestTamplateSearch(Login_Doctor.driver, logger).sendKeys(labTamplateText);		
		Assert.assertTrue(lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty());
	}
	@Test(priority = 18, groups = { "Regression", "LabPeriodicity" }, description = "LTP_29")
	public void repeat_Follow_Up_TestB() throws Exception {
		logger = Reports.extent.createTest("To verify newly entered values are updating with the old values");
		String number = RandomStringUtils.randomNumeric(1);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		Web_GeneralFunctions.wait(2);
		js.executeScript("arguments[0].click()", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		lab.getEveryTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown = new Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		textDropdown.selectByIndex(2);
		lab.getForTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown = new Select(lab.getForDropdown(Login_Doctor.driver, logger));
		forTextDropdown.selectByIndex(3);
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);		
		Assert.assertTrue(lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("value").isEmpty());
	}
	@Test(priority = 19, groups = { "Regression", "LabPeriodicity" }, description = "LT_30", dependsOnMethods = "repeat_Follow_Up_TestB", enabled = false)
	public void repeat_follow_up_clear_option() throws Exception {
		logger = Reports.extent.createTest("To verify by clicking on Clear option, can able to turn off the follow up reminder for lab test B");
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		Web_GeneralFunctions.scrollToElement(lab.getSearchTestOrScanLast(Login_Doctor.driver, logger), Login_Doctor.driver);
		js.executeScript("arguments[0].click()", lab.getCleanOptionBtn(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(2);
		Assert.assertTrue(lab.getSearchTestOrScanLast(Login_Doctor.driver, logger).getAttribute("Value").isEmpty(), "Search Test or Scan field is cleared");
		Assert.assertTrue(lab.getNotesTestOrScanLast(Login_Doctor.driver, logger).getAttribute("Value").isEmpty(), "Notes Test or Scan field is cleared");
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		
	}
}