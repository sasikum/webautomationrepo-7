
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertThrows;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
//import com.sun.mail.imap.protocol.SearchSequence;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;

public class LabTestModuleTest{

	public static Integer row = 0;
	public List<String> SavedLabTest = new ArrayList<String>();
	public List<String> SavedLabTestNotes = new ArrayList<String>();
	public static String createTemplate = "";
	
	ExtentTest logger;
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Lab Tests");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public synchronized void PrintButtonInSpecifyTest()throws Exception{
		
		logger = Reports.extent.createTest("EMR Specify test print button");
		
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getSpecifyTestPrintButton(Login_Doctor.driver, logger), "Click Print button in specify test module", Login_Doctor.driver, logger);
		//please add thread.sleep in respective method call
	}
	
	public synchronized void clickAddAsTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test Add as template button");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getAddAsTemplate(Login_Doctor.driver, logger), "Click Add as template", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void clickAddRow()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test Add row button");
		LabTestPage lab = new LabTestPage();
	//	System.out.println("inside click add");
		Web_GeneralFunctions.click(lab.getAddRow(Login_Doctor.driver, logger), "Click Add row button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void clickSaveAsTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test Save Template Button");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getSaveTemplate(Login_Doctor.driver, logger), "Click Save Template button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void closeSaveTemplatePopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR Close Save Template Pop up");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getTemplateCloseButton(Login_Doctor.driver, logger),"Close  save template pop up", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	public synchronized void deleteButton()throws Exception{
		
		logger = Reports.extent.createTest("EMR Delete Row");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getDeleteButton(row, Login_Doctor.driver, logger),"Click Delete button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	// move to lab test module
	@Test(groups= {"Regression","Login"},priority=901)
	public synchronized void consultation_014()throws Exception{
		
		logger = Reports.extent.createTest("EMR Lab Test");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.moveToLabTestModule(Login_Doctor.driver, logger),"Move to specify test module", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("901");
	}
	
	
	
	@Test(groups= {"Regression","Login"},priority=902)
	public synchronized void clickPrintWithoutData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test print button with out data");
		LabTestPage lab = new LabTestPage();
		PrintButtonInSpecifyTest();// Click on the print button
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger), "Get Lab test empty data error message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Add atleast one test before printing")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(5000);
		System.out.println("902");

	}
	
	@Test(groups= {"Regression","Login"},priority=903)
	public synchronized void clickAddAsTemplateWithEmptyData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test Add as template button with empty data");
		LabTestPage lab = new LabTestPage();
		clickAddAsTemplate();// click on the add template of lab test
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger), "Get Lab test empty data error message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Please specify atleast one Test/Scan")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(5000);
		System.out.println("903");

	}
	
	@Test(groups= {"Regression","Login"},priority=904)
	public synchronized void clickAddRowWithEmptyData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Specify test Add row button with empty data");
		LabTestPage lab = new LabTestPage();
		clickAddRow();
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger), "Get Lab test empty data error message", Login_Doctor.driver, logger);
		//System.out.println("empty row data : "+ text);
		if(text.equalsIgnoreCase("Please fill all mandatory fields before adding")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(5000);
		System.out.println("904");

	}
	
	// filling data lab testing name and note
	@Test(groups= {"Regression","Login"},priority=905)
	public synchronized void Consultation_015()throws Exception{
		
		logger = Reports.extent.createTest("EMR Select Lab test from auto populate drop down");
		
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.selectLabTestFromDropDown(Login_Doctor.driver, row, "", logger), "select lab test from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String availableRow = Web_GeneralFunctions.getAttribute(lab.getAvailableRow(row+1,Login_Doctor.driver, logger), "id", "Get id of the row", Login_Doctor.driver, logger) ;
		row = Integer.parseInt(availableRow);
		String getValueOfLabTest = Web_GeneralFunctions.getAttribute(lab.getLabTest(Login_Doctor.driver, row, logger),"value", "Get Lab Test Value", Login_Doctor.driver, logger);
		
		SavedLabTest.add(getValueOfLabTest);
		SavedLabTestNotes.add("EMPTY");
		System.out.println("saved lab test : "+SavedLabTest);
		System.out.println("905");

	}
	
	
	@Test(groups= {"Regression","Login"},priority=906)
	public synchronized void saveLabTestByRightMenu()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Lab test by clicking right menu");
		
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.saveLabTest(Login_Doctor.driver, logger), "Save lab test by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		String text = Web_GeneralFunctions.getText(lab.getSuccessMessage(Login_Doctor.driver, logger),"Get Lab test save message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Test/Scan details have been saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("906");

	}
	
	@Test(groups= {"Regression","Login"},priority=907)
	public synchronized void Consultation_016() throws Exception{
		
		
		logger = Reports.extent.createTest("EMR Search and select lab tests");
		consultation_014(); // move to lab test
		String searchTestNames = "renal";
		
		LabTestPage lab = new LabTestPage();
		clickAddRow();
		
		row++;
		
		Web_GeneralFunctions.click(lab.selectLabTestFromDropDown(Login_Doctor.driver, row, searchTestNames, logger), "select lab test from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);	
		
		String getValueOfLabTest = Web_GeneralFunctions.getAttribute(lab.getLabTest(Login_Doctor.driver, row, logger),"value", "Get Lab Test Value", Login_Doctor.driver, logger);
		SavedLabTest.add(getValueOfLabTest);
		SavedLabTestNotes.add("EMPTY");
		System.out.println("907");

		
	}
	
	public synchronized void saveLabTestByScrollDown()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Lab test by scrolling down");
		
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.saveLabTest(Login_Doctor.driver, logger),"Save lab test by scroll down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(lab.getSuccessMessage(Login_Doctor.driver, logger),"Get Lab test save message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Test/Scan details have been saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(3000);
	}
	
	@Test(groups= {"Regression","Login"},priority=908)
	public synchronized void Consultation_017()throws Exception{
		//boolean notesStatus = false;
		boolean testNameStatus = false;
		logger = Reports.extent.createTest("EMR Verify Specify test print pdf");
		GlobalPrintTest gp = new GlobalPrintTest();
		LabTestPage lab = new LabTestPage();
		//click specify test Print button
		//consultation_014();
		PrintButtonInSpecifyTest();
		Thread.sleep(5000);
		String pdfContent = gp.getPDFPage();
		
		int i=0;
		String searchTestNamePDF = "";
		while(i<SavedLabTest.size()) {
		//	System.out.println(SavedLabTest.get(i)+" "+SavedLabTestNotes.get(i));
			searchTestNamePDF = SavedLabTest.get(i);
			if(!SavedLabTestNotes.get(i).equalsIgnoreCase("EMPTY")) {
				searchTestNamePDF += " "+SavedLabTestNotes.get(i);
			}
			
			if(pdfContent.contains(searchTestNamePDF)) {
				testNameStatus = true;
			}else {
				testNameStatus = false;
				break;
			}
			i++;
		}
		
		if(testNameStatus == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("908");

	}
	
	
	
	@Test(groups= {"Regression","Login"},priority=909)
	public synchronized void clickPrintButtonWithoutEnteringTestNames()throws Exception{
		logger = Reports.extent.createTest("EMR Print Lab test without test name and with notes");
		
		consultation_014();// move to test lab
		LabTestPage lab = new LabTestPage();
		
		row++;
		String notes = "Dummy value";
		clickAddRow();
		System.out.println("row : "+row);
		Web_GeneralFunctions.sendkeys(lab.getNotes(Login_Doctor.driver, row, logger), notes,"Sending notes value", Login_Doctor.driver, logger);
		Thread.sleep(1000);	
		
		//click print button
		PrintButtonInSpecifyTest();
		Thread.sleep(1000);
		
		String text = Web_GeneralFunctions.getText(lab.getPrintErrorMessage(Login_Doctor.driver, logger),"Get Test Name Mandatory error message", Login_Doctor.driver, logger);
		//System.out.println("text before printing : "+text);
		if(text.equalsIgnoreCase("Please fill all mandatory fields before printing")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(5000);
		System.out.println("909");

	}
	
	
	@Test(groups= {"Regression","Login"},priority=910)
	public synchronized void SaveLabTestWithoutEnteringTestNames()throws Exception{
		logger = Reports.extent.createTest("EMR Save  Lab test without test name and with notes");
		
		consultation_014();
		LabTestPage lab = new LabTestPage();
		
		//Save Lab Test by right menu without test names;
		Web_GeneralFunctions.click(lab.saveLabTest(Login_Doctor.driver, logger), "Save lab test by right menu", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger),"Get Test Name Mandatory error message", Login_Doctor.driver, logger);
		//System.out.println("text before adding : "+text);
		if(text.equalsIgnoreCase("Please fill all mandatory fields")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(5000);
		System.out.println("910");

	}
	
	
	
	@Test(groups= {"Regression","Login"},priority=911)
	public synchronized void clickAddAsTemplateButtonWithoutEnteringTestNames()throws Exception{
		logger = Reports.extent.createTest("EMR Click Add as template Lab test button without test name and with notes");
		
		consultation_014();
		LabTestPage lab = new LabTestPage();
		
		clickAddAsTemplate();
		String testScanTemplateName = RandomStringUtils.randomAlphabetic(7);
		Web_GeneralFunctions.sendkeys(lab.getTestScanTemplateNameField(Login_Doctor.driver, logger), testScanTemplateName, "Sending random value in template name field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		//click save as template
		clickSaveAsTemplate();
		
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger),"Get Test Name Mandatory error message", Login_Doctor.driver, logger);
	//	System.out.println("text before adding : "+text);
		if(text.equalsIgnoreCase("Please fill all mandatory fields before adding")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		closeSaveTemplatePopUp();
		Thread.sleep(5000);
		System.out.println("911");

	}
	
	@Test(groups= {"Regression","Login"},priority=912)
	public synchronized void clickDeleteButton()throws Exception{
		
		logger = Reports.extent.createTest("EMR Delete Lab Test Row");
		LabTestPage lab = new LabTestPage();
		deleteButton();
		Thread.sleep(1000);
		System.out.println("912");

	}
	
	@Test(groups= {"Regression","Login"},priority=913)
	public synchronized void duplicateLabTest()throws Exception{
		
		logger = Reports.extent.createTest("EMR Duplicate lab test");
		LabTestPage lab = new LabTestPage();
		clickAddRow();
		
		String searchTestName = SavedLabTest.get(0);
		Web_GeneralFunctions.sendkeys(lab.getLabTest(Login_Doctor.driver, row, logger), searchTestName,"Sending duplicate value", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.click(lab.selectLabTestFromDropDown(Login_Doctor.driver, row, searchTestName, logger),"Sending duplicate values in lab test name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
		Web_GeneralFunctions.click(lab.getNotes(Login_Doctor.driver, row, logger), "Click Notes section", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger),"Get Duplicate warning message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Duplicate test/scan is found")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("913");

	}
	
	@Test(groups= {"Regression","Login"},priority=914)
	public synchronized void addTestNameWithNotes()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Free text in lab test name and notes");
		
		LabTestPage lab = new LabTestPage();
		String freeText = RandomStringUtils.randomAlphabetic(9);
		String notes = "Lab test notes";
		
		Web_GeneralFunctions.sendkeys(lab.getNotes(Login_Doctor.driver, row, logger), notes, "Sending notes value to text box", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		//row+=2;
		
		Web_GeneralFunctions.sendkeys(lab.getLabTest(Login_Doctor.driver, row, logger),freeText,"Sending free text values in test name field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		SavedLabTest.add(freeText);
		SavedLabTestNotes.add(notes);
		System.out.println("914");

	}
	
	@Test(groups= {"Regression","Login"},priority=915)
	public synchronized void saveTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Lab test template");
		LabTestPage lab = new LabTestPage();
		clickAddAsTemplate();
		Web_GeneralFunctions.wait(2);
		//saveTemplate();
		Web_GeneralFunctions.clearWebElement(lab.getTestScanTemplateNameField(Login_Doctor.driver, logger), "Clear the already present value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		createTemplate = RandomStringUtils.randomAlphabetic(5);
		Web_GeneralFunctions.sendkeys(lab.getTestScanTemplateNameField(Login_Doctor.driver, logger), createTemplate, "Sending Template Name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		clickSaveAsTemplate();
		Web_GeneralFunctions.wait(2);
		String text = Web_GeneralFunctions.getText(lab.getSaveTemplateSuccessMessage(Login_Doctor.driver, logger),"Get Save template success message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Template saved successfully.")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(3);
		System.out.println("915");

	}
	
	@Test(groups= {"Regression","Login"},priority=916)
	public synchronized void selectTemplateFromDropDown()throws Exception{
		
		logger = Reports.extent.createTest("EMR Add lab test from template");
		consultation_014();
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.click(lab.getLabTestTemplateFromDropDown("", Login_Doctor.driver, logger), "Select any one of the template from drop down list", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger), "Get Success message", Login_Doctor.driver, logger);
		if(text.contains("template added successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("916");

	}
	
	
	@Test(groups= {"Regression","Login"},priority=917)
	public synchronized void createdTemplateUpdatedInTemplateList()throws Exception{
		
		logger = Reports.extent.createTest("EMR Verify created template present in template Drop down and duplicate check from template");
		LabTestPage lab = new LabTestPage();
		Web_GeneralFunctions.clearWebElement(lab.getLabTestTemplateSearch(Login_Doctor.driver, logger),"Clear the value in search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(lab.getLabTestTemplateFromDropDown(createTemplate, Login_Doctor.driver, logger), "Select any one of the template from drop down list", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(lab.getMessage(Login_Doctor.driver, logger), "Get Success message", Login_Doctor.driver, logger);
		String message =  "template added successfully";
		if(text.contains(message)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		System.out.println("917");

	}

	@Test(groups= {"Regression","Login"},priority=918)
	public synchronized void LabTestVerifyInPrintAll()throws Exception{
		boolean status = false;
		
		logger = Reports.extent.createTest("EMR Print All PDF Check");
		GlobalPrintTest gpt = new GlobalPrintTest();
		LabTestPage lab = new LabTestPage();
		SavedLabTest = lab.getData(Login_Doctor.driver, logger);
		gpt.moveToPrintModule();
		gpt.printAll();
		String pdfContent = gpt.getPDFPage();
		System.out.println("pdf content : "+pdfContent);

		/*
		Web_GeneralFunctions.click(symptom.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "Move to symptom module", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		*/
		
		boolean testNameStatus = false;
		
		if(pdfContent.contains("Lab Tests")) {
 			
			int i=0;
			String searchTestNamePDF = "";                  
			//while(i<SavedLabTest.size()) {
			System.out.println("........SavedLabTestNotes"+SavedLabTestNotes);
			while(i<SavedLabTestNotes.size()) {

				System.out.println(SavedLabTest.get(i)+" "+SavedLabTestNotes.get(i));
				searchTestNamePDF = SavedLabTest.get(i);
//				if(!SavedLabTestNotes.get(i).equalsIgnoreCase("EMPTY")) {
//					//searchTestNamePDF += " (Notes:"+SavedLabTestNotes.get(i)+")";
//					searchTestNamePDF += SavedLabTestNotes.get(i);
//
//				}
				System.out.println(searchTestNamePDF);
				if(pdfContent.contains(searchTestNamePDF)) {
					testNameStatus = true;
				}else {
					testNameStatus = false;
					System.out.println("failed search test name : "+searchTestNamePDF);
					break;
				}
				i++;
			}
		}else {
			//assertTrue(false);
			testNameStatus = false;
		}
		
		if(testNameStatus == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(2000);
		
		System.out.println("918");

		
	}
	
	@Test(groups= {"Regression","Login"},priority=919)
	public synchronized void LabTestVerifyInPrint()throws Exception{
		boolean status = false;
		
		logger = Reports.extent.createTest("EMR Print PDF Check");
		GlobalPrintTest gpt = new GlobalPrintTest();
		LabTestPage lab = new LabTestPage();
		
		gpt.print();
		String pdfContent = gpt.getPDFPage();
		
	System.out.println("pdfContent"+pdfContent);
		
		boolean testNameStatus = false;
		
		if(pdfContent.contains("Lab Tests")) {
			int i=0;
			while(i<SavedLabTest.size()) {
				System.out.println("lab test : "+SavedLabTest.get(i));
				String searchTestNamePDF = SavedLabTest.get(i);
				
				System.out.println("...."+searchTestNamePDF);
				if(pdfContent.contains(searchTestNamePDF)) {
					testNameStatus = true;
				}else {
					testNameStatus = false;
					break;
				}	
				i++;
			}
			
		}else {
			testNameStatus = false;
		}
		
		if(testNameStatus == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(2000);
		System.out.println("919");

	}
	
	
	public synchronized String getSavedData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Get Data from lab test module");
		LabTestPage lab = new LabTestPage();
		String testNames = "";
		List<String> testNamesList = lab.getData(Login_Doctor.driver, logger);
		ReusableData.savedConsultationData.put("LabTest", (ArrayList<String>) testNamesList);
		int i = 0;
		while(i<testNamesList.size()) {
			testNames += testNamesList.get(i)+",";
			i++;
		}
		
		return testNames;
	}
	
	
	@Test(groups= {"Regression","Login"},priority=920)
	public synchronized void deteleSavedData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Delete saved lab test from lab test module");
		LabTestPage lab = new LabTestPage();
		//move to lab test module
		consultation_014();
		Random r = new Random();
		int deleterowIndex = r.nextInt(SavedLabTest.size());
		String deletedRowId =  Web_GeneralFunctions.getAttribute(lab.getAvailableRow(deleterowIndex, Login_Doctor.driver, logger),"id","Get id of lab test row", Login_Doctor.driver, logger);
		row = Integer.parseInt(deletedRowId);
		if(row == 0) {
			row +=1;
		}
		String deletedtestName = Web_GeneralFunctions.getAttribute(lab.getLabTest(Login_Doctor.driver, row, logger), "value","Get deleted lab test", Login_Doctor.driver, logger);
		//delete particular row
		clickDeleteButton();
		//save the changes
		saveLabTestByRightMenu();
		//move back to lab test module
		consultation_014();
		
		String testNames = getSavedData();
		
		if(!testNames.contains(deletedtestName)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("failed");
		System.out.println("920");

	}
	
	
}
