package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.AfterCheckedOutPages;
import com.tatahealth.EMR.pages.Consultation.LabResultPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.VitalsPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PostCheckOutTest{
	
	ExtentTest logger;
	ConsultationTest currentTest = new ConsultationTest();
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("PostCheckOut");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}
	
/*Commenting this as the start consultation is already handled by the before class method loginAndStartConsultation
 *That method creates a new consultation if the status is not "Consulting"
 * @Test(groups= {"Regression","Login"},priority=950)
	public synchronized void bookAppointmentForSameUser()throws Exception{
		
		logger = Reports.extent.createTest("EMR book appointemnt for same user");
		ConsultationTest consult = new ConsultationTest();
		consult.startConsultation();
		Thread.sleep(3000);
	}*/
	
	@Test(groups= {"Regression","Login"},priority=818)
	public synchronized void verifyTimeLineOfPreviousVisit()throws Exception {
		
		logger = Reports.extent.createTest("EMR verify timeline");
		AfterCheckedOutPages pages = new AfterCheckedOutPages();
		GlobalPrintTest gpTest = new GlobalPrintTest();

		Web_GeneralFunctions.click(pages.moveToStartPointOfTimeLine(Login_Doctor.driver, logger), "click timeline", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		
		
		String timelineText = Web_GeneralFunctions.getText(pages.getTimeLineData(Login_Doctor.driver, logger), "Get timeline data", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("timeline data : "+ timelineText);
		
		/*Web_GeneralFunctions.click(pages.getTimeLinePrintAll(Login_Doctor.driver, logger), "click print all", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		String pdfContent = gpTest.getPDFPage();
		
		Web_GeneralFunctions.click(pages.getTimeLinePrintSummaryPDF(Login_Doctor.driver, logger), "click print all", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		pdfContent = gpTest.getPDFPage();*/
		
		Web_GeneralFunctions.click(pages.getCloseTimeLineModal(Login_Doctor.driver, logger), "close modal", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	@Test(groups= {"Regression","Login"},priority=819)
	public synchronized void verifyVitals()throws Exception{
		logger = Reports.extent.createTest("EMR verify Vitals");
		VitalsPage vPages = new VitalsPage();
		VitalsTest vTest = new VitalsTest();
		
		String weightText = Web_GeneralFunctions.getAttribute(vPages.getWeightElement(Login_Doctor.driver, logger), "value", "get weight field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
		String heightText = Web_GeneralFunctions.getAttribute(vPages.getHeightElement(Login_Doctor.driver, logger), "value", "get height field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
		if(!weightText.isEmpty() && !heightText.isEmpty()) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
	}
	
	@Test(groups= {"Regression","Login"},priority=820)
	public synchronized void repeatPrescription()throws Exception{
		logger = Reports.extent.createTest("EMR repeat prescription");
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTest pTest = new PrescriptionTest();
		ReferralTest rTest = new ReferralTest();
		//rTest.moveToReferral();
		pTest.moveToPrescriptionModule();
		pTest.repeatPrescription();
		pTest.repeatPrescriptionNo();
		pTest.repeatPrescriptionYes();
	
	}
	
}
