package com.tatahealth.API.Core;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
//import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Encryption {
	private String initVector = "6d000b484e9611e8";
	private IvParameterSpec ivParameterSpec;
	private SecretKeySpec secretKeySpec;
	private Cipher cipher;
	
	public String encrypt(String toBeEncrypt, String secretKey) throws Exception {
		
		encryptDecrypt(initVector, secretKey);
		
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		byte[] encrypted = cipher.doFinal(toBeEncrypt.getBytes());
		String ret = Base64.encodeBase64String(encrypted);
		return ret;
		}
	
	public String decrypt(String encrypted,String secretKey) throws Exception{
		encryptDecrypt(initVector, secretKey);
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
	    byte[] decryptedBytes = cipher.doFinal(Base64.decodeBase64(encrypted));
	    return new String(decryptedBytes);
	}
	
	public void encryptDecrypt(String initVector,String secretKey) throws Exception{
		ivParameterSpec = new IvParameterSpec(initVector.getBytes("UTF-8"));
		secretKeySpec = new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		
	}
	
		
}
