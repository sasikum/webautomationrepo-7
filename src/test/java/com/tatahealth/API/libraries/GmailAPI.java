package com.tatahealth.API.libraries;

import org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.Gmail.Users.Messages.Send;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class GmailAPI {

	/**
	 * @author Bala Yaswanth
	 * @param userId
	 * @param messageId
	 * @throws Exception
	 * 
	 */
	public static void getAttachments(String userId, String messageId) throws Exception {
		Gmail service = Quickstart.getGmailService();
		Message message = service.users().messages().get(userId, messageId).execute();
		List<MessagePart> parts = message.getPayload().getParts();
		for (MessagePart part : parts) {
			if (part.getFilename() != null && part.getFilename().length() > 0) {
				String filename = part.getFilename();
				String attId = part.getBody().getAttachmentId();
				MessagePartBody attachPart = service.users().messages().attachments().get(userId, messageId, attId)
						.execute();
				// Base64 base64Url = new Base64(true);
				byte[] fileByteArray = Base64.decodeBase64(attachPart.getData());
				String dirpath = "./apk_directory";
				File Dir = new File(dirpath);
				File[] files = Dir.listFiles();
				for (File file : files) {
					file.delete();
				}
				FileOutputStream fileOutFile = new FileOutputStream("./apk_directory/" + filename);
				fileOutFile.write(fileByteArray);
				fileOutFile.close();
			}
		}
	}

	public static List<Message> listMessagesMatchingQuery(String userId, String query) throws Exception {

		Gmail service = Quickstart.getGmailService();
		ListMessagesResponse response = service.users().messages().list(userId).setQ(query).execute();
		List<Message> messages = new ArrayList<Message>();
		while (response.getMessages() != null) {
			messages.addAll(response.getMessages());
			if (response.getNextPageToken() != null) {
				String pageToken = response.getNextPageToken();
				response = service.users().messages().list(userId).setQ(query).setPageToken(pageToken).execute();
			} else {
				break;
			}
		}
		for (Message message : messages) {
			System.out.println(message.toPrettyString());
		}
		return messages;
	}

	public static Message getMessage(String userId, String messageId) throws Exception {

		Gmail service = Quickstart.getGmailService();
		Message message = service.users().messages().get(userId, messageId).execute();
		System.out.println("Message snippet: " + message.getSnippet());
		return message;

	}

	public static MimeMessage createEmail(List<String> to, String from, String subject, String bodyText)
			throws MessagingException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage email = new MimeMessage(session);

		email.setFrom(new InternetAddress(from));

		for (int i = 0; i < to.size(); i++) {
			email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to.get(i)));
		}
		email.setSubject(subject);
		email.setText(bodyText);
		// System.out.println(email);
		return email;
	}

	public static Message createMessageWithEmail(MimeMessage email) throws MessagingException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		email.writeTo(baos);
		String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
		Message message = new Message();
		message.setRaw(encodedEmail);
		return message;
	}

	public static void sendMessage(String userId, MimeMessage email) throws Exception {
		Gmail service = Quickstart.getGmailServiceForServiceAccount(userId);
		Message message = createMessageWithEmail(email);
		Send e1 = service.users().messages().send(userId, message);
		e1.execute();
	}

	public static MimeMessage createEmailWithAttachment(List<String> to, String from, String subject, String bodyText,
			File file) throws MessagingException, IOException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage email = new MimeMessage(session);

		email.setFrom(new InternetAddress(from));
		for (int n = 0; n < to.size(); n++) {

			email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to.get(n)));

		}
		email.setSubject(subject);

		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(bodyText, "text/plain");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(mimeBodyPart);

		mimeBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(file);

		mimeBodyPart.setDataHandler(new DataHandler(source));
		mimeBodyPart.setFileName(file.getName());

		multipart.addBodyPart(mimeBodyPart);
		email.setContent(multipart);

		return email;
	}

}
