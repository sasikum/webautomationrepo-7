package com.tatahealth.DB.Scripts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.tatahealth.API.Scripts.GeneralAPIFunctions;


public class OTP {
	
	private List<NameValuePair> header;
	
	public String getQAOTP(String MobileNumber) throws ClientProtocolException, JSONException, IOException, SAXException, ParserConfigurationException {
		
		String OTP = null;
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Ameyo"));
		header.add(new BasicNameValuePair("vendor_token","a47160c0-b0bb-4447-9912-352feeceef37"));
		
		String URL = "https://apiqa.tatahealth.com/consumerOnboarding/api/v1/getOTP/"+MobileNumber;
		
		GeneralAPIFunctions api = new GeneralAPIFunctions();
		JSONObject a = api.getRequestWithHeaders(URL, header);
		JSONObject b = a.getJSONObject("responseData");
		//System.out.println(b.toString());
		OTP = Integer.toString(b.getInt("otpCode"));
		
        return OTP;
	}
	
	public String getStagingOTP(String MobileNumber) throws ClientProtocolException, JSONException, IOException, SAXException, ParserConfigurationException {
		
		String OTP = null;
        
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Ameyo"));
		header.add(new BasicNameValuePair("vendor_token","b2953f46-3d79-464a-b038-23bf78033592"));
		
		String URL = "https://apigtw.tatahealth.com/consumerOnboarding/api/v1/getOTP/"+MobileNumber;
		
		GeneralAPIFunctions api = new GeneralAPIFunctions();
		JSONObject a = api.getRequestWithHeaders(URL, header);
		JSONObject b = a.getJSONObject("responseData");
		//System.out.println(b.toString());
		OTP = Integer.toString(b.getInt("otpCode"));
		
		
        return OTP;
	}
	
	
	public String getProdOTP(String MobileNumber) throws ClientProtocolException, JSONException, IOException, SAXException, ParserConfigurationException {
		
		String OTP = null;
        
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Ameyo"));
		header.add(new BasicNameValuePair("vendor_token",""));//Talk to deiva for prod key
		
		String URL = "https://api.tatahealth.com/consumerOnboarding/api/v1/getOTP/"+MobileNumber;
		
		GeneralAPIFunctions api = new GeneralAPIFunctions();
		JSONObject a = api.getRequestWithHeaders(URL, header);
		JSONObject b = a.getJSONObject("responseData");
		//System.out.println(b.toString());
		OTP = Integer.toString(b.getInt("otpCode"));
		
		
        return OTP;
	}


}
